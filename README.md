# plpbt

plpbt  boot for older computers

![](plpbt.png)





``` 
menuentry 'Linux Sarge Antic (Grub-1)' { 
        set root='hd0,msdos2'
        linux16  /boot/vmlinuz root=/dev/sdb2 rw  
        initrd16 /boot/initrd.img
}




menuentry 'Linux Plpbt Boot (Grub-1)' { 
        set root='hd0,msdos1'
        linux16  /root/plpbt.bin 
}
``` 




